class Order < ActiveRecord::Base
  attr_accessible :date, :total, :order_no

  has_many :order_lines
  has_many :products, through: :order_lines

  validates_presence_of :date, :total, :order_no
  validates :total, :numericality => { :greater_than_or_equal_to => 0}

end
