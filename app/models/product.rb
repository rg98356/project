class Product < ActiveRecord::Base
  attr_accessible :price, :product_name
  has_many :order_lines
  has_many :inventories

  validates_presence_of :price, :product_name
  validates :price, :numericality => { :greater_than_or_equal_to => 0}
end
