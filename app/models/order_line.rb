class OrderLine < ActiveRecord::Base
  attr_accessible :order_id, :product_id, :quantity
  belongs_to :order
  belongs_to :product


  validates_presence_of :order_id, :product_id, :quantity
  validates :quantity, :numericality => { :greater_than_or_equal_to => 0}
end
