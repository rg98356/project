class CreateSearches < ActiveRecord::Migration
  def change
    create_table :searches do |t|
      t.string :product_name
      t.integer :quantity
      t.date :start_date
      t.date :end_date
      t.float :cost

      t.timestamps
    end
  end
end
